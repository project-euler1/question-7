import math

def is_prime(n):
    for i in range(2, int(math.sqrt(n))+1):
        if n % i == 0: return False
    return True

def get_prime(n):
    counter = 1
    i = 3
    last_prime = 2
    while(counter < n):
        if is_prime(i):
            counter += 1
            last_prime = i
        i+=2
    return last_prime

print(get_prime(10001))